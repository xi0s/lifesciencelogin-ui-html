/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/script.js":
/*!**************************!*\
  !*** ./src/js/script.js ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("const init = (e) => {\n  // Selectors\n  const institution_search_input = document.querySelector(\n    \".js-institution-search-input\"\n  );\n\n  const institution_search_dropdown = document.querySelector(\n    \".js-institution-search-dropdown\"\n  );\n\n  const institution_dropdown_items = document.querySelectorAll(\n    \".js-institution-item\"\n  );\n\n  const js_institution_btn = document.querySelector(\".js-institution-btn\");\n\n  const js_continue_provider_btn = document.querySelector(\n    \".js-continue-provider-btn\"\n  );\n\n  const js_provider_items = document.querySelectorAll(\".js-provider-item\");\n\n  // sign in logic\n  if (\n    institution_search_input &&\n    institution_search_dropdown &&\n    institution_dropdown_items &&\n    js_institution_btn\n  ) {\n    // Handlers\n    const onFocus = (e) => {\n      institution_search_dropdown.classList.remove(\"hide\");\n    };\n\n    const onBlur = (e) => {\n      institution_search_dropdown.classList.add(\"hide\");\n    };\n\n    const onItemSelected = (item) => (e) => {\n      const txt_node = item.querySelector(\"span\");\n\n      if (txt_node) {\n        institution_search_input.value = txt_node.textContent;\n\n        js_institution_btn.classList.remove(\"hide\");\n      }\n    };\n\n    // Listeners\n    institution_search_input.addEventListener(\"focus\", onFocus);\n    institution_search_input.addEventListener(\"blur\", onBlur);\n\n    institution_dropdown_items.forEach((item) => {\n      item.addEventListener(\"mousedown\", onItemSelected(item));\n    });\n  }\n\n  // id provider logic\n  if (js_continue_provider_btn && js_provider_items) {\n    // Handlers\n    const onProvidorSelected = (item) => (e) => {\n      const check = item.querySelector(\".check-bold\");\n\n      if (check) {\n        check.classList.toggle(\"disabled\");\n      }\n\n      js_continue_provider_btn.classList.toggle(\"btn-disabled\");\n    };\n\n    // Listener\n    js_provider_items.forEach((item) => {\n      item.addEventListener(\"mousedown\", onProvidorSelected(item));\n    });\n  }\n};\n\nwindow.addEventListener(\"load\", init);\n\n\n//# sourceURL=webpack:///./src/js/script.js?");

/***/ }),

/***/ "./src/scss/styles.scss":
/*!******************************!*\
  !*** ./src/scss/styles.scss ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony default export */ __webpack_exports__[\"default\"] = (__webpack_require__.p + \"public/css/styles.css\");\n\n//# sourceURL=webpack:///./src/scss/styles.scss?");

/***/ }),

/***/ 0:
/*!*******************************************************!*\
  !*** multi ./src/js/script.js ./src/scss/styles.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("__webpack_require__(/*! ./src/js/script.js */\"./src/js/script.js\");\nmodule.exports = __webpack_require__(/*! ./src/scss/styles.scss */\"./src/scss/styles.scss\");\n\n\n//# sourceURL=webpack:///multi_./src/js/script.js_./src/scss/styles.scss?");

/***/ })

/******/ });