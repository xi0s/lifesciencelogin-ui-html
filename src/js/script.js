const init = (e) => {
  // Selectors
  const institution_search_input = document.querySelector(
    ".js-institution-search-input"
  );

  const institution_search_dropdown = document.querySelector(
    ".js-institution-search-dropdown"
  );

  const institution_dropdown_items = document.querySelectorAll(
    ".js-institution-item"
  );

  const js_institution_btn = document.querySelector(".js-institution-btn");

  const js_continue_provider_btn = document.querySelector(
    ".js-continue-provider-btn"
  );

  const js_provider_items = document.querySelectorAll(".js-provider-item");

  // sign in logic
  if (
    institution_search_input &&
    institution_search_dropdown &&
    institution_dropdown_items &&
    js_institution_btn
  ) {
    // Handlers
    const onFocus = (e) => {
      institution_search_dropdown.classList.remove("hide");
    };

    const onBlur = (e) => {
      institution_search_dropdown.classList.add("hide");
    };

    const onItemSelected = (item) => (e) => {
      const txt_node = item.querySelector("span");

      if (txt_node) {
        institution_search_input.value = txt_node.textContent;

        js_institution_btn.classList.remove("hide");
      }
    };

    // Listeners
    institution_search_input.addEventListener("focus", onFocus);
    institution_search_input.addEventListener("blur", onBlur);

    institution_dropdown_items.forEach((item) => {
      item.addEventListener("mousedown", onItemSelected(item));
    });
  }

  // id provider logic
  if (js_continue_provider_btn && js_provider_items) {
    // Handlers
    const onProvidorSelected = (item) => (e) => {
      const check = item.querySelector(".check-bold");

      if (check) {
        check.classList.toggle("disabled");
      }

      js_continue_provider_btn.classList.toggle("btn-disabled");
    };

    // Listener
    js_provider_items.forEach((item) => {
      item.addEventListener("mousedown", onProvidorSelected(item));
    });
  }
};

window.addEventListener("load", init);
